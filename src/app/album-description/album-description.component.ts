import { Component, OnInit } from '@angular/core';
import {Album} from '../album';
import {ActivatedRoute} from '@angular/router';
import {AlbumService} from '../album.service';

@Component({
  selector: 'app-album-description',
  templateUrl: './album-description.component.html',
  styleUrls: ['./album-description.component.scss']
})
export class AlbumDescriptionComponent implements OnInit {

  album: Album;

  constructor(private route: ActivatedRoute, private albumService: AlbumService) { }

  ngOnInit() {
      /* Récupération id route + infos album */
      const id = this.route.snapshot.paramMap.get('id');
      this.album = this.albumService.getAlbum(id);
  }
}
