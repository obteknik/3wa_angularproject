import { Injectable } from '@angular/core';
import { Album, List } from './album';
import { ALBUM_LISTS, ALBUMS } from './mock-albums';
import {environment} from "../environments/environment";
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AlbumService {

  private _albums: Album[] = ALBUMS; // _ convention private et protected
  private _albumList: List[] = ALBUM_LISTS;

  subjectAlbum = new Subject<Album>();
  sendCurrentNumberPage = new Subject<number>(); // pour mettre à jour la pagination

  constructor() { }

  //retourne la liste des albums par ordre DESC duration
  getAlbums(): Album[] {
    return this._albums.sort(
      (a, b) => { return b.duration - a.duration }
    );
  }

  //retourne un album désigné
  getAlbum(id: string): Album  {
    return this._albums.find(album => album.id === id);
  }

  //retourne la liste des chansons
  getAlbumList(id: string): List {
    return this._albumList.find(list => list.id === id);
  }

  //retourne le nombre total d'albums
  count(): number{
    return this._albums.length;
  }

  /*** PARTIE RECHERCHE ****/
  //Retourne la recherche et le résultat de recherche [CORRECTION]
  /*search(word: string): Album[] {
      let re = new RegExp(word.trim(), 'g');

      // filter permet de filter un tableau avec un test dans le test ci-dessous on vérifie
      // deux choses : 1/ que album.title.match(re) n'est pas vide si c'est le contraire alors c'est pas faux
      // et 2/ si on a trouver des titres qui matchaient/t avec la recherche
      return this._albums.filter(album => album.title.match(re) && album.title.match(re).length > 0) ;
  }*/

    currentPage(page: number) {
        return this.sendCurrentNumberPage.next(page);
    }

  search(word: string):any {
      let results: Album[] = [];
      this._albums.forEach(album => {
          for (let elem in album) {
              if(album[elem].toString().includes(word)) {
                  results.push(album);
              }
          }
      });
      return [results, word];
  }

  /*** PARTIE PAGINATION ****/
  //retourne la liste des albums par ordre DESC duration, uniquement entre start & end
 paginate(start: number, end: number):Album[]{
  return this._albums.sort(
    (a, b) => { return b.duration - a.duration }
  ).slice(start, end); /* fn slice() */
 }

 //retourne la variable d'environnement Nb d'albums par page
 paginateNumberPage():number{
    if ( typeof environment.numberPage == 'undefined' )
        throw "Attention la pagination n'est pas définie" ;

        return environment.numberPage ;
    }

    /*** PARTIE LECTEUR AUDIO ****/
    //Lancer la lecture audio
    switchOn(album: Album) {
        this._albums.forEach(
            a => {
                if (a.id === album.id) album.status = 'on';
                else
                    a.status = 'off';
            }

        );

        this.subjectAlbum.next(album); // Observer puscher les informations
    }

    //Stopper la lecture audio
    // Cette méthode n'a pas besoin d'émettre une information à l'Observable
    switchOff(album: Album) {
        this._albums.forEach(
            a => {
                a.status = 'off';
            }
        );
    }
}