import {animate, state, style, transition, trigger} from "@angular/animations";

export const ANIMATION_DETAIL_ALBUM = [
    trigger('myAnimationDetailAlbum', [
        state('open', style({
            //height: '200px',
            //opacity: 0.8,
            //backgroundColor: 'lightgrey'
        })),
        state('closed', style({
            //height: '100px',
            //opacity: 0.5,
            backgroundColor: 'dodgerblue',
                //right: '100%'
        })),
        // Transition entre les 2 états
        transition('open => closed', [
            animate('2s')
        ])
    ])
];