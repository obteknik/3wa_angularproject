import {Component } from '@angular/core';
import { interval, Observable} from 'rxjs';
import {map, take} from "rxjs/operators";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})

export class AppComponent {
  title: string = 'Start Music';
  pipeTimer;
  timer: Date;

    ngOnInit() {
        /* Compteur en secondes */
        const counter: any = interval(1000);

        /* Transformation de la donnée en date */
        this.pipeTimer = counter.pipe(
            take(100), /* Arrêt à 100*/
            map((counter: number) => {
                let date = new Date(0);
                date.setSeconds(counter);
                date.setHours(-0.1);
                return date;
            })
        );

        this.pipeTimer.subscribe(
            date => this.timer = date
        );
    }
}
