import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {NgForm} from '@angular/forms';
import {AlbumService} from '../album.service';
import {Album} from '../album';
import {ALBUMS} from '../mock-albums';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})

export class SearchComponent implements OnInit {

    @Output() onSearch: EventEmitter<Album[]> = new EventEmitter();
    albums: Album[] = ALBUMS;

  constructor(private albumService: AlbumService) { }

  ngOnInit() {
      this.albums = this.albumService.paginate(0,this.albumService.paginateNumberPage());
  }

  /* Soumission du formulaire */
  onSubmit(form: NgForm): void {
      /* Utilisation du service pour la recherche */
      const resultSearch = this.albumService.search(form.value['word']);

      /* Emission du résultat de recherche pour le parent */
      this.onSearch.emit(resultSearch);
  }

}
