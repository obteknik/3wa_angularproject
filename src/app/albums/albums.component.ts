import { Component, OnInit } from '@angular/core';
import { Album } from '../album';
import { ALBUMS } from '../mock-albums';
import { AlbumService } from '../album.service';

@Component({
  selector: 'app-albums',
  templateUrl: './albums.component.html',
  styleUrls: ['./albums.component.scss'],
})

export class AlbumsComponent implements OnInit {

  titlePage: string = "Liste des albums";
  albums: Album[] = ALBUMS;
  selectedAlbum : Album; // pour passer l'album sélectionné dans la liste à la vue détail
  status: string = null; // pour gérer l'affichage des caractères [play]
  countAlbums = this.albumService.count();
  searchTerm: string;
  language: string;

  constructor(private albumService: AlbumService) { }

  ngOnInit() {
    this.albums = this.albumService.paginate(0,this.albumService.paginateNumberPage());
  }

  onSelect(album: Album) {
    this.selectedAlbum = album;
  }

  /* Bouton Play d'album détail */
    playParent($event){
        /* Pour affichage [Played] pour l'album concerné */
        this.status = $event.id;

        /*if($event.status =='off'){
            this.albumService.switchOn($event);
            console.log('off/je lance : ' + $event.title + ' - status = ' + $event.status);

        } else {
            this.albumService.switchOff($event);
            console.log('on/je stoppe :' + $event.title + ' - status = ' + $event.status);
        }*/
        this.albumService.switchOn($event);
    }

  /* Bouton de recherche */
  searchParent($event){
      this.albums = $event[0];
      this.searchTerm = $event[1];
      this.countAlbums = this.albumService.count();
  }

  /* Pagination */
  paginate($event) {
        this.albums = this.albumService.paginate($event.start, $event.end);
  }
}
