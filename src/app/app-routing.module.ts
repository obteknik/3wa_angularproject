import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {LoginComponent} from './login/login.component';
import {AlbumDescriptionComponent} from './album-description/album-description.component';
import {AlbumsComponent} from './albums/albums.component';

/*Définition de la constante pour les routes*/
const albumsRoutes: Routes = [
    {
        path: '',
        redirectTo: '/albums',
        pathMatch: 'full'
    },
    {
        path: 'albums',
        component: AlbumsComponent
    },
    {
        path: 'album/:id',
        component: AlbumDescriptionComponent
    },
    {
        path: 'login',
        component: LoginComponent
    }
];

@NgModule({
  imports: [RouterModule.forRoot(albumsRoutes)], /*Chargement des routes dans l'application*/
  exports: [RouterModule]
})

export class AppRoutingModule { }
