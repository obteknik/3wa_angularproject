/*
Nous disposons d’une liste d’utilisateurs users. Souscrivez à l’Observable of et
affichez les users dont le score est supérieur à 100 en mettant chaque prénom
avec une majuscule à la première lettre.
source.subscribe(
// affichez le resultat
)
 */
const { of } = require('rxjs');
const { map, filter } = require('rxjs/operators');

const users = of( { id: 1, name: 'alan', score: 50 },
{ id: 2, name: 'albert', score: 150 },
{ id: 3, name: 'xavier', score: 0 },
{ id: 4, name: 'benoit', score: 5 },
{ id: 5, name: 'phil', score: 17 },
{ id: 6, name: 'sophie', score: 45 },
{ id: 7, name: 'stephan', score: 900 },
{ id: 8, name: 'elie', score: 178 },
{ id: 9, name: 'tony', score: 1005 },
{ id: 10, name: 'robert', score: 11 },
{ id: 11, name: 'gerard', score: 8 },
{ id: 12, name: 'sandra', score: 6 },
{ id: 13, name: 'caroline', score: 23 });

const filterUser = users.pipe(filter(user => user.score > 100));

const subscribeOne = filterUser.subscribe(val =>
    (console.log(val.name.charAt(0).toUpperCase() +
        val.name.slice(1))));

// se désinscrire au bout de 10 secondes
// exécute la fonction d'annulation de l'Observable
setTimeout(() => {
    subscribeOne.unsubscribe();
}, 10000);
