/*
Dans le fichier ex1_points.js vous avez une liste de points à deux coordonnées.
Déterminez le point le plus éloigné du point O(0,0) à l’aide de la fonction max
et la méthode sqrt de Math (M ath.sqrt(x
2 + y
2
)) pour calculer la distance d’un
point à O(0,0).
Vous afficherez le résultat à l’aide d’un console.log
Remarques : l’objet of émet une série de valeurs, c’est un Observable.
 */

const { of } = require('rxjs');
const { max } = require('rxjs/operators');

// liste de points // of c'est un Observable 
const source = of(
    { x: 10.5, y: -10.6 }, // 1
    { x: 5.5, y: 8.3 },  // 2
    { x: -7.3, y: 3.3 }, // 3
    { x: 8.9, y: -2.6 } // 4
);

// mapping data  TODO filtre
const pipeMax = source.pipe(
    max((a,b) => Math.sqrt(a.x*a.x + a.y*a.y) - Math.sqrt(b.x*b.x + b.y*b.y)
    )
);

// s'inscrire TODO et afficher les données
const subscribeOne = pipeMax.subscribe(
    num => console.log(`coordonnées les plus lointaines de (0,0) : ${num.x} ${num.y}`
    ));

// se désinscrire au bout de 10 secondes
// exécute la fonction d'annulation de l'Observable
setTimeout(() => {
    subscribeOne.unsubscribe();
}, 10000);