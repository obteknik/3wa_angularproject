import { Component, OnInit, OnChanges, Input, Output, EventEmitter } from '@angular/core';
import { Album, List } from '../album';
import { AlbumService } from '../album.service';
import {ANIMATION_DETAIL_ALBUM} from "../animation";

@Component({
  selector: 'app-album-details',
  templateUrl: './album-details.component.html',
  styleUrls: ['./album-details.component.scss'],
  animations: ANIMATION_DETAIL_ALBUM
})

export class AlbumDetailsComponent implements OnInit, OnChanges {

  @Output() onPlayAlbum: EventEmitter<Album> = new EventEmitter();
  @Output() onStopPlayAlbum: EventEmitter<Album> = new EventEmitter();
  @Input() album: Album;

  songs: List;
  isActive: boolean;
  showplayer: boolean;
  stateButton: boolean = false;

  constructor(private AlbumService: AlbumService) {}

  ngOnInit() {
      this.stateButton = true;
  }

  ngOnChanges() {
    if (this.album) { //vérif album cliqué

        /* Récupération de la liste des chansons de l'album */
      //this.songs = this.albumLists.find(elem => elem.id === this.album.id);
      this.songs = this.AlbumService.getAlbumList(this.album.id);

      /* Animation */
      let animation = setTimeout(() =>{
            this.isActive = true;
      },2000) /* au bout de 2 secondes(true) => Ds vue=> [@myAnimationDetailAlbum]="isActive ? 'open' : 'closed'*/
    }
  }

  /* Bouton Play */
  playAlbum(album: Album) {
      this.onPlayAlbum.emit(album); // émettre de l'album x vers le parent
  }

  stopPlayAlbum(album: Album) {

        this.onStopPlayAlbum.emit(album); // émettre de l'album x vers le parent

    }
}
